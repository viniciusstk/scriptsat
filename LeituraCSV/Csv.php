<?php


class Csv
{
    public function leCsv($arquivo)
    {
        $row = 1;
        $array = array();
        if ($handle = fopen($arquivo, "r")) {
            while ($data = fgetcsv($handle, 1000, "|")){
                foreach ($data as $line){
                    if ($row == 1){
                        array_shift($data); //remove headers
                        $row ++;
                    }else{
                        $var = explode(';', $line);
                        $item = new stdClass();
                        $item->numeroDc = $var[0];
                        $item->cnpjEmit = $var[1];
                        $item->emitente = $var[2];
                        $item->dataEmissao = $var[3];
                        $item->valor = $var[4];
                        $item->chave = str_replace("'", "", $var[5]);
                        $item->ufEmit = $var[6];
                        $item->situacao = $var[7];
                        $item->operacao = $var[8];

                        $array[] = $item;
                    }
                }
            }
            fclose($handle);
            return $array;
        }

    }
}