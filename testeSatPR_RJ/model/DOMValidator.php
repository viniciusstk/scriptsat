<?php


class DOMValidator
{
    /**
     * @var string
     */
    protected $feedSchema = "";
    /**
     * @var int
     */
    public $feedErrors = 0;
    /**
     * Formatted libxml Error details
     *
     * @var array
     */
    public $errorDetails;
    /**
     * Validation Class constructor Instantiating DOMDocument
     *
     * @param \DOMDocument $handler [description]
     */
    public function __construct()
    {
        $this->handler = new \DOMDocument('1.0', 'utf-8');
    }
    function descompactaZIP()
    {

    }
    /**
     * @param \libXMLError object $error
     *
     * @return string
     */
    private function libxmlDisplayError($error)
    {
        $errorString = "Error $error->code in $error->file (Line:{$error->line}):";
        $errorString .= trim($error->message);
        return $errorString;
    }
    /**
     * @return array
     */
    private function libxmlDisplayErrors()
    {
        $errors = libxml_get_errors();
        $result    = [];
        foreach ($errors as $error) {
            $result[] = $this->libxmlDisplayError($error);
        }
        libxml_clear_errors();
        return $result;
    }


    /**
     * Validate Incoming Feeds against Listing Schema
     *
     * @param resource $feeds
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function validaPR()
    {
        $this->feedSchema = dirname(dirname(__FILE__)) . '/arquivos/LayoutNF4/procNFe_v4.00.xsd';
        libxml_disable_entity_loader(false);
        $contents = file_get_contents(dirname(dirname(__FILE__)) . '/arquivos/41190806092854000121550010000242351002438612.arquivos',FILE_USE_INCLUDE_PATH);


        $tmpDom = new DOMDocument();
        $tmpDom->loadXML($contents, LIBXML_NOBLANKS);

        $auxDom = new DOMDocument();
        $auxDom->loadXML($tmpDom->saveXML($tmpDom->getElementsByTagName('procNFe')->item(0)));
        $auxDom->save(dirname(dirname(__FILE__)) . "/tmpXml/arquivos" . uniqid() . ".arquivos");

//        $tmpDom->save($tmpDom->getElementsByTagName('procNFe')->item(0));

//        error_log($tmpContent);
//
          //Troca procNFe por NFeproc
//        if (preg_match("/^\<procNFe[\w\W]+\<\/procNFe\>$/", $tmpContent)) {
//            $tmpContent = preg_replace('/^\<procNFe/', '<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00"', preg_replace('/\<\/procNFe\>$/', '</nfeProc>', $tmpContent));
//        }
//        error_log($tmpContent);
//
//
//        $this->handler->loadXML($tmpContent, LIBXML_NOBLANKS);
//
          //Valida schema
//        if (!$this->handler->schemaValidate($this->feedSchema)) {
//            $this->errorDetails = $this->libxmlDisplayErrors();
//            $this->feedErrors   = 1;
//        } else {
//            //The file is valid
//            return true;
//        }
    }

    public function validaRJ()
    {
        try
        {
            $unzip = new Descompact();
            $unzip->DescompactZip(__DIR__ . '/../arquivos/tmpZip',__DIR__ . '/../arquivos/tmpXml',__DIR__ . '/../arquivos/tmpZip/zipError');

        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
    /**
     * Display Error if Resource is not validated
     *
     * @return array
     */
    public function displayErrors()
    {
        return $this->errorDetails;
    }
}
